package com.whitebox.atm.models;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "bank_accounts")
public class BankAccount {
    @Id
    @GeneratedValue
    private Integer id;

    @Column(unique = true)
    @NotBlank(message = "The iban is required.")
    private String iban;

    @NotBlank(message = "The holder name is required.")
    private String holderName;

    @NotNull(message = "The balance is required.")
    @Min(value = 0, message = "The balance must be minimum 0.")
    private Integer balance;

    public BankAccount() {
    }

    public BankAccount(String iban, String holderName, Integer balance) {
        this.iban = iban;
        this.holderName = holderName;
        this.balance = balance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "iban='" + iban + '\'' +
                ", holderName='" + holderName + '\'' +
                ", balance=" + balance +
                '}';
    }
}
