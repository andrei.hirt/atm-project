package com.whitebox.atm.models;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Counter {
    private int amountToCountFor;

    private List<Banknote> banknotes;

    private int[] minimumOfNotesForEachAmount;

    private int[][] stateOfEachNoteLeftForEachAmount;

    private int[] countOfEachNoteForAmount;

    public void init(int amountToCountFor, List<Banknote> banknotes) {
        Collections.sort(banknotes);
        this.banknotes = banknotes;
        minimumOfNotesForEachAmount = new int[amountToCountFor + 1];
        stateOfEachNoteLeftForEachAmount = new int[amountToCountFor + 1][banknotes.size()];
        countOfEachNoteForAmount = new int[banknotes.size()];
        this.amountToCountFor = amountToCountFor;

        Arrays.fill(minimumOfNotesForEachAmount, Integer.MAX_VALUE);
        minimumOfNotesForEachAmount[0] = 0;

        for (int i = 0; i < banknotes.size(); i++) {
            stateOfEachNoteLeftForEachAmount[0][i] = banknotes.get(i).getCount();
        }
    }

    public void findMinimumNumberOfBanknotes() {
        for (int currAmount = 1; currAmount <= amountToCountFor; currAmount++) {
            for (int noteIdx = 0; noteIdx < banknotes.size(); noteIdx++) {
                if (banknotes.get(noteIdx).isSuitable(currAmount)) {
                    if (canUseBanknoteForAmount(noteIdx, currAmount)) {
                        countBanknoteForAmount(noteIdx, currAmount);
                    }
                }
            }
        }
    }

    public boolean canUseBanknoteForAmount(int noteIdx, int amount) {
        int subAmount = amount - banknotes.get(noteIdx).getValue();
        int minOfNotesSubAmount = minimumOfNotesForEachAmount[subAmount];

        return minOfNotesSubAmount != Integer.MAX_VALUE && minOfNotesSubAmount + 1 < minimumOfNotesForEachAmount[amount]
                && stateOfEachNoteLeftForEachAmount[subAmount][noteIdx] > 0;
    }

    public void countBanknoteForAmount(int noteIdx, int amount) {
        int subAmount = amount - banknotes.get(noteIdx).getValue();
        int minOfNotesSubAmount = minimumOfNotesForEachAmount[subAmount];

        minimumOfNotesForEachAmount[amount] = minOfNotesSubAmount + 1;
        System.arraycopy(stateOfEachNoteLeftForEachAmount[subAmount], 0, stateOfEachNoteLeftForEachAmount[amount],0, banknotes.size());
        stateOfEachNoteLeftForEachAmount[amount][noteIdx]--;
    }

    public void finishWithdrawal() {
        if (isWithdrawalSuccessful()) {
            saveCountOfEachNoteForAmount();
        }
    }

    public boolean isWithdrawalSuccessful() {
        return minimumOfNotesForEachAmount[amountToCountFor] != Integer.MAX_VALUE;
    }

    public void saveCountOfEachNoteForAmount() {
        for (int noteIdx = 0; noteIdx < banknotes.size(); noteIdx++) {
            countOfEachNoteForAmount[noteIdx] = banknotes.get(noteIdx).getCount() - stateOfEachNoteLeftForEachAmount[amountToCountFor][noteIdx];
        }
    }

    public int getAmountToCountFor() {
        return amountToCountFor;
    }

    public int[] getMinimumOfNotesForEachAmount() {
        return minimumOfNotesForEachAmount;
    }

    public int[][] getStateOfEachNoteLeftForEachAmount() {
        return stateOfEachNoteLeftForEachAmount;
    }

    public int[] getStateOfEachNoteLeftForAmount(int amount) {
        return stateOfEachNoteLeftForEachAmount[amount];
    }

    public int[] getCountOfEachNoteForAmount() {
        return countOfEachNoteForAmount;
    }

    public int getMinimumOfNotesForAmount(int amount) {
        return minimumOfNotesForEachAmount[amount];
    }

    public List<Banknote> getBanknotes() {
        return banknotes;
    }
}
