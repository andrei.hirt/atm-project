package com.whitebox.atm.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "atm_funds")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Banknote implements Comparable<Banknote> {
    @Id
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private BanknoteType type;

    @Column(name = "_count")
    @Min(value = 0, message = "Count must be positive.")
    @NotNull(message = "Count is required.")
    private Integer count;

    public Banknote() {
        this.count = 0;
    }

    public Banknote(BanknoteType type, Integer count) {
        this.type = type;
        this.count = count;
    }

    public int compareTo(Banknote banknote) {
        return this.getValue().compareTo(banknote.getValue());
    }

    public BanknoteType getType() {
        return type;
    }

    public void setType(BanknoteType type) {
        this.type = type;
    }

    public Integer getValue() {
        return type.getValue();
    }

    public String getName() {
        return type.getName();
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public boolean isSuitable(Integer amount) {
        return type.getValue() <= amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Banknote other = (Banknote) o;
        return this.type == other.type && this.count.equals(other.count);
    }

    @Override
    public String toString() {
        return "Banknote{" +
                "type=" + type +
                ", count=" + count +
                '}';
    }
}

