package com.whitebox.atm.models;

public class FactoryBanknote {
    public Banknote createBanknote(int value, int count) {
        if (count < 0) {
            throw new IllegalArgumentException("Count must be positive");
        }

        switch (value) {
            case 1:
                return new Banknote(BanknoteType.ONE_RON, count);
            case 5:
                return new Banknote(BanknoteType.FIVE_RON, count);
            case 10:
                return new Banknote(BanknoteType.TEN_RON, count);
            case 20:
                return new Banknote(BanknoteType.TWENTY_RON, count);
            case 50:
                return new Banknote(BanknoteType.FIFTY_RON, count);
            case 100:
                return new Banknote(BanknoteType.ONE_HUNDRED_RON, count);
            default:
                throw new IllegalArgumentException("Unknown banknote value " + value + ".");
        }
    }
}
