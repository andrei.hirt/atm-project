package com.whitebox.atm.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = BanknoteType.CustomEnumDeserializer.class)
public enum BanknoteType {
    ONE_RON(1, "1 RON"),
    FIVE_RON(5, "5 RON"),
    TEN_RON(10, "10 RON"),
    TWENTY_RON(20, "20 RON"),
    FIFTY_RON(50, "50 RON"),
    ONE_HUNDRED_RON(100, "100 RON");

    private final Integer value;
    private final String name;

    BanknoteType(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public static class CustomEnumDeserializer extends StdDeserializer<BanknoteType> {
        public CustomEnumDeserializer() {
            super(BanknoteType.class);
        }

        @Override
        public BanknoteType deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);
            String unit = node.asText();

            for (BanknoteType banknoteType : BanknoteType.values()) {
                if (banknoteType.name.equals(unit)) {
                    return banknoteType;
                }
            }
            return null;
        }
    }

    @Override
    public String toString() {
        return "BanknoteType{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }
}
