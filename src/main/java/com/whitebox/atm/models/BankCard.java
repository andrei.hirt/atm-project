package com.whitebox.atm.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "bank_cards")
public class BankCard {
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "holder_user_id")
    @JsonBackReference
    private User holderUser;

    @NotBlank(message = "The card number is required.")
    private String cardNumber;

    @NotBlank(message = "The holder name is required.")
    private String holderName;

    @NotBlank(message = "The expiry date is required.")
    private String expiryDate;

    @NotBlank(message = "The issuing bank is required.")
    private String issuingBank;

    @NotBlank(message = "The cvv is required.")
    @Size(min = 3, max = 3, message = "The cvv must contain 3 digits.")
    private String cvv;

    @NotBlank(message = "The pin is required.")
    @Size(min = 4, max = 4, message = "The pin must contain 4 digits.")
    private String pin;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_account_id")
    private BankAccount bankAccount;

    public BankCard() {
    }

    public BankCard(User holderUser, String cardNumber, String holderName, String expiryDate, String issuingBank, String cvv, String pin, BankAccount bankAccount) {
        this.holderUser = holderUser;
        this.cardNumber = cardNumber;
        this.holderName = holderName;
        this.expiryDate = expiryDate;
        this.issuingBank = issuingBank;
        this.cvv = cvv;
        this.pin = pin;
        this.bankAccount = bankAccount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getHolderUser() {
        return holderUser;
    }

    public void setHolderUser(User holderUser) {
        this.holderUser = holderUser;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getIssuingBank() {
        return issuingBank;
    }

    public void setIssuingBank(String issuingBank) {
        this.issuingBank = issuingBank;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public BankAccount getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    @Override
    public String toString() {
        return "BankCard{" +
                "cardNumber='" + cardNumber + '\'' +
                ", holderName='" + holderName + '\'' +
                ", expiryDate='" + expiryDate + '\'' +
                ", issuingBank='" + issuingBank + '\'' +
                ", CVVCode=" + cvv +
                ", PINNumber=" + pin +
                ", bankAccount=" + bankAccount +
                '}';
    }
}
