package com.whitebox.atm.controllers;

import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteType;
import com.whitebox.atm.services.BanknoteService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class BanknoteController {
    private final BanknoteService banknoteService;

    public BanknoteController(BanknoteService banknoteService) {
        this.banknoteService = banknoteService;
    }

    @GetMapping("/atm/currency")
    public ResponseEntity<List<BanknoteType>> getAtmCurrency() {
        return ResponseEntity.ok(banknoteService.getAtmCurrency());
    }

    @GetMapping("/atm/funds")
    public ResponseEntity<List<Banknote>> getAtmFunds() {
        return ResponseEntity.ok(banknoteService.getAtmFunds());
    }

    @PostMapping("/atm/funds")
    public ResponseEntity<Banknote> addBanknote(@Valid @RequestBody Banknote banknote) {
        return new ResponseEntity<>(banknoteService.addBanknote(banknote), HttpStatus.CREATED);
    }

    @PostMapping("/atm/funds/list")
    public ResponseEntity<List<Banknote>> addListOfBanknotes(@Valid @RequestBody List<Banknote> banknoteList) {
        return new ResponseEntity<>(banknoteService.addListOfBanknotes(banknoteList), HttpStatus.CREATED);
    }

    @PutMapping("/atm/funds")
    public ResponseEntity<Banknote> increaseBanknote(@Valid @RequestBody Banknote banknote) {
        return ResponseEntity.ok(banknoteService.increaseBanknote(banknote));
    }
}
