package com.whitebox.atm.controllers;

import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.services.AtmService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class AtmController {
    private final AtmService atmService;

    public AtmController(AtmService atmService) {
        this.atmService = atmService;
    }

    @GetMapping("/atm/withdraw/{bankCardId}")
    public ResponseEntity<List<Banknote>> withdraw(@PathVariable Integer bankCardId, @RequestParam Integer amount) {
        return ResponseEntity.ok(atmService.withdraw(bankCardId, amount));
    }

    @PostMapping("/atm/deposit/{bankCardId}")
    public ResponseEntity<Integer> deposit(@PathVariable Integer bankCardId, @RequestParam Integer amount) {
        return ResponseEntity.ok(atmService.deposit(bankCardId, amount));
    }

    @GetMapping("/atm/check-balance/{bankCardId}")
    public ResponseEntity<Integer> checkBalance(@PathVariable Integer bankCardId) {
        return ResponseEntity.ok(atmService.checkBalance(bankCardId));
    }
}
