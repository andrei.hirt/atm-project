package com.whitebox.atm.controllers;

import com.whitebox.atm.models.BankCard;
import com.whitebox.atm.services.BankCardService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class BankCardController {
    private final BankCardService bankCardService;

    public BankCardController(BankCardService bankCardService) {
        this.bankCardService = bankCardService;
    }

    @GetMapping("/bank-cards")
    public ResponseEntity<List<BankCard>> getAllBankCards() {
        return ResponseEntity.ok(bankCardService.getAllBankCards());
    }

    @GetMapping("/bank-cards/{id}")
    public ResponseEntity<BankCard> getBankCard(@PathVariable Integer id) {
        return ResponseEntity.ok(bankCardService.getBankCard(id));
    }

    @GetMapping("/users/{userId}/bank-cards")
    public ResponseEntity<List<BankCard>> getAllBankCardsByUserId(@PathVariable Integer userId) {
        return ResponseEntity.ok(bankCardService.getAllBankCardsByUserId(userId));
    }

    @GetMapping("/users/{userId}/bank-cards/{bankCardId}")
    public ResponseEntity<BankCard> getBankCardByUserId(@PathVariable Integer userId, @PathVariable Integer bankCardId) {
        return ResponseEntity.ok(bankCardService.getBankCardByUserId(userId, bankCardId));
    }

    @PostMapping("/users/{userId}/bank-cards")
    public ResponseEntity<BankCard> addBankCard(@PathVariable Integer userId, @Valid @RequestBody BankCard bankCard) {
        return new ResponseEntity<>(bankCardService.addBankCard(userId, bankCard), HttpStatus.CREATED);
    }
}
