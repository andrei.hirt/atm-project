package com.whitebox.atm.controllers;

import com.whitebox.atm.models.BankAccount;
import com.whitebox.atm.services.BankAccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class BankAccountController {
    private final BankAccountService bankAccountService;

    public BankAccountController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    @GetMapping("/bank-accounts")
    public ResponseEntity<List<BankAccount>> getAllBankAccounts() {
        return ResponseEntity.ok(bankAccountService.getAllBankAccounts());
    }

    @GetMapping("/bank-accounts/{id}")
    public ResponseEntity<BankAccount> getBankAccount(@PathVariable Integer id) {
        return ResponseEntity.ok(bankAccountService.getBankAccount(id));
    }
}
