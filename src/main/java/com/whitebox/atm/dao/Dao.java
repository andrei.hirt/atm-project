package com.whitebox.atm.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public abstract class Dao<T, H> {
    @Autowired
    public EntityManager entityManager;

    public abstract List<T> getAll();
    public abstract T getById(H id);
}
