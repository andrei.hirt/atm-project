package com.whitebox.atm.dao;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.ResourceNotFoundException;
import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteType;
import com.whitebox.atm.models.QBanknote;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class BanknoteDaoImpl extends Dao<Banknote, BanknoteType> {
    private final QBanknote qBanknote = QBanknote.banknote;

    @Override
    public List<Banknote> getAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(qBanknote).fetch();
    }

    @Override
    public Banknote getById(BanknoteType type) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        Banknote banknote = queryFactory.selectFrom(qBanknote).where(qBanknote.type.eq(type)).fetchFirst();
        if (banknote == null) {
            throw new ResourceNotFoundException("Banknote with type " + type + " not found!");
        }
        return banknote;
    }

    public Banknote save(Banknote banknote) {
        entityManager.persist(banknote);
        return banknote;
    }

    public List<Banknote> save(List<Banknote> banknoteList) {
        for (Banknote banknote : banknoteList) {
            entityManager.persist(banknote);
        }
        return banknoteList;
    }

    public Banknote increaseBanknote(Banknote banknote) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        Banknote updatedBanknote = queryFactory.selectFrom(qBanknote).where(qBanknote.type.eq(banknote.getType())).fetchFirst();
        if (updatedBanknote == null) {
            throw new ResourceNotFoundException("Banknote with type " + banknote.getType() + " not found!");
        }

        queryFactory.update(qBanknote).where(qBanknote.type.eq(banknote.getType()))
                .set(qBanknote.count, updatedBanknote.getCount() + banknote.getCount()).execute();

        updatedBanknote.setCount(updatedBanknote.getCount() + banknote.getCount());
        return updatedBanknote;
    }

    public void updateAtmFunds(List<Banknote> banknotesAtm, int[] countOfEachAtmBanknote) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        for (int noteIdx = 0; noteIdx < banknotesAtm.size(); noteIdx++) {
            queryFactory.update(qBanknote).where(qBanknote.type.eq(banknotesAtm.get(noteIdx).getType()))
                    .set(qBanknote.count, countOfEachAtmBanknote[noteIdx]).execute();
        }
    }
}
