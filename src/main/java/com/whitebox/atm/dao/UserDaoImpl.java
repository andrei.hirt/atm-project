package com.whitebox.atm.dao;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.ResourceNotFoundException;
import com.whitebox.atm.models.QUser;
import com.whitebox.atm.models.User;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class UserDaoImpl extends Dao<User, Integer> {
    private final QUser qUser = QUser.user;

    @Override
    public List<User> getAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(qUser).fetch();
    }

    @Override
    public User getById(Integer id) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        User user = queryFactory.selectFrom(qUser).where(qUser.id.eq(id)).fetchFirst();
        if (user == null) {
            throw new ResourceNotFoundException("User with id " + id + " not found!");
        }
        return user;
    }

    public User save(User user) {
        entityManager.persist(user);
        return user;
    }
}
