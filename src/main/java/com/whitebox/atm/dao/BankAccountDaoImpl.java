package com.whitebox.atm.dao;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.ResourceNotFoundException;
import com.whitebox.atm.models.BankAccount;
import com.whitebox.atm.models.QBankAccount;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class BankAccountDaoImpl extends Dao<BankAccount, Integer> {
    private final QBankAccount qBankAccount = QBankAccount.bankAccount;

    @Override
    public List<BankAccount> getAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(qBankAccount).fetch();
    }

    @Override
    public BankAccount getById(Integer id) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        BankAccount bankAccount = queryFactory.selectFrom(qBankAccount).where(qBankAccount.id.eq(id)).fetchFirst();
        if (bankAccount == null) {
            throw new ResourceNotFoundException("Bank Account with id " + id + " not found!");
        }
        return bankAccount;
    }

    public void updateBankAccountBalance(Integer id, int newBalance) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        BankAccount bankAccount = queryFactory.selectFrom(qBankAccount).where(qBankAccount.id.eq(id)).fetchFirst();

        if (bankAccount == null) {
            throw new ResourceNotFoundException("Bank Account with id " + id + " not found!");
        }

        queryFactory.update(qBankAccount).where(qBankAccount.id.eq(id)).set(qBankAccount.balance, newBalance).execute();
    }
}
