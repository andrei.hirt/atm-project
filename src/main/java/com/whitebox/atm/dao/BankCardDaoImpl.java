package com.whitebox.atm.dao;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.whitebox.atm.exceptions.ResourceNotFoundException;
import com.whitebox.atm.models.BankCard;
import com.whitebox.atm.models.QBankCard;
import com.whitebox.atm.models.QUser;
import com.whitebox.atm.models.User;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class BankCardDaoImpl extends Dao<BankCard, Integer> {
    private final QBankCard qBankCard = QBankCard.bankCard;

    private final QUser qUser = QUser.user;

    @Override
    public List<BankCard> getAll() {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return queryFactory.selectFrom(qBankCard).fetch();
    }

    @Override
    public BankCard getById(Integer id) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        BankCard bankCard = queryFactory.selectFrom(qBankCard).where(qBankCard.id.eq(id)).fetchFirst();
        if (bankCard == null) {
            throw new ResourceNotFoundException("Bank Card with id " + id + " not found!");
        }
        return bankCard;
    }

    public List<BankCard> getAllBankCardsByUserId(Integer userId) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        User user = queryFactory.selectFrom(qUser).where(qUser.id.eq(userId)).fetchFirst();
        if (user == null) {
            throw new ResourceNotFoundException("User with id " + userId + " not found!");
        }
        return queryFactory.selectFrom(qBankCard).where(qBankCard.holderUser.id.eq(userId)).fetch();
    }

    public BankCard getBankCardByUserId(Integer userId, Integer bankCardId) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        User user = queryFactory.selectFrom(qUser).where(qUser.id.eq(userId)).fetchFirst();
        if (user == null) {
            throw new ResourceNotFoundException("User with id " + userId + " not found!");
        }
        BankCard bankCard = queryFactory.selectFrom(qBankCard).where(qBankCard.holderUser.id.eq(userId).and(qBankCard.id.eq(bankCardId))).fetchFirst();
        if (bankCard == null) {
            throw new ResourceNotFoundException("Bank Card with id " + bankCardId + " not found!");
        }
        return bankCard;
    }

    public BankCard save(Integer userId, BankCard bankCard) {
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        QUser qUser = QUser.user;

        User user = queryFactory.selectFrom(qUser).where(qUser.id.eq(userId)).fetchFirst();
        if (user == null) {
            throw new ResourceNotFoundException("User with id " + userId + " not found!");
        }
        user.addBankCard(bankCard);
        bankCard.setHolderUser(user);
        entityManager.persist(bankCard);
        return bankCard;
    }
}
