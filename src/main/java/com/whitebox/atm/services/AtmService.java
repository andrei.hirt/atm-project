package com.whitebox.atm.services;

import com.whitebox.atm.exceptions.BalanceOperationsException;
import com.whitebox.atm.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AtmService {
    @Autowired
    private BanknoteService banknoteService;

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private BankCardService bankCardService;

    public List<Banknote> withdraw(Integer bankCardId, Integer amount) {
        BankCard bankCard = bankCardService.getBankCard(bankCardId);
        if (amount <= 0) {
            throw new BalanceOperationsException("Error! You can't withdraw " + amount + " RON from your bank account!");
        }
        if (amount > bankCard.getBankAccount().getBalance()) {
            throw new BalanceOperationsException("Error! Insufficient bank account funds!");
        }

        List<Banknote> banknotes = banknoteService.getAtmFunds();
        Counter counter = new Counter();
        counter.init(amount, banknotes);
        counter.findMinimumNumberOfBanknotes();
        counter.finishWithdrawal();

        int[] countOfEachBanknoteForAmount = counter.getCountOfEachNoteForAmount();
        int[] valueOfEachBanknote = banknotes.stream().map(Banknote::getValue).mapToInt(x -> x).toArray();
        int[] countOfEachAtmBanknote =  counter.getStateOfEachNoteLeftForAmount(amount);

        if (withdrawResultIsValid(countOfEachBanknoteForAmount, valueOfEachBanknote, amount)) {
            bankAccountService.updateBankAccountBalance(bankCard.getBankAccount().getId(), bankCard.getBankAccount().getBalance() - amount);
            banknoteService.updateAtmFunds(counter.getBanknotes(), countOfEachAtmBanknote);
            return banknoteService.getBanknotesWithdraw(valueOfEachBanknote, countOfEachBanknoteForAmount);
        } else {
            throw new BalanceOperationsException("Error! Insufficient ATM funds!");
        }
    }

    public boolean withdrawResultIsValid(int[] countOfEachBanknote, int[] valueOfEachBanknote, Integer amount) {
        int sum = 0;
        for (int noteIdx = 0; noteIdx < valueOfEachBanknote.length; noteIdx++) {
            sum += countOfEachBanknote[noteIdx] * valueOfEachBanknote[noteIdx];
        }

        return sum == amount;
    }

    public Integer deposit(Integer bankCardId, Integer amount) {
        BankCard bankCard = bankCardService.getBankCard(bankCardId);

        if (amount <= 0) {
            throw new BalanceOperationsException("Error! You can't deposit " + amount + " RON to your bank account!");
        } else {
            bankAccountService.updateBankAccountBalance(bankCard.getBankAccount().getId(), bankCard.getBankAccount().getBalance() + amount);
            return amount;
        }
    }

    public Integer checkBalance(Integer bankCardId) {
        BankCard bankCard = bankCardService.getBankCard(bankCardId);
        return bankCard.getBankAccount().getBalance();
    }
}
