package com.whitebox.atm.services;

import com.whitebox.atm.models.*;
import com.whitebox.atm.dao.BankCardDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BankCardService {
    @Autowired
    private BankCardDaoImpl bankCardDao;

    public List<BankCard> getAllBankCards() {
        return bankCardDao.getAll();
    }

    public BankCard getBankCard(Integer id) {
        return bankCardDao.getById(id);
    }

    public List<BankCard> getAllBankCardsByUserId(Integer userId) {
        return bankCardDao.getAllBankCardsByUserId(userId);
    }

    public BankCard getBankCardByUserId(Integer userId, Integer bankCardId) {
        return bankCardDao.getBankCardByUserId(userId, bankCardId);
    }

    public BankCard addBankCard(Integer userId, BankCard bankCard) {
        return bankCardDao.save(userId, bankCard);
    }
}
