package com.whitebox.atm.services;

import com.whitebox.atm.models.BankAccount;
import com.whitebox.atm.dao.BankAccountDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class BankAccountService {
    @Autowired
    private BankAccountDaoImpl bankAccountDao;

    public List<BankAccount> getAllBankAccounts() {
        return bankAccountDao.getAll();
    }

    public BankAccount getBankAccount(Integer id) {
        return bankAccountDao.getById(id);
    }

    public void updateBankAccountBalance(Integer id, int newBalance) {
        bankAccountDao.updateBankAccountBalance(id, newBalance);
    }
}
