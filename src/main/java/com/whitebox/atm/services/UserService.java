package com.whitebox.atm.services;

import com.whitebox.atm.models.User;
import com.whitebox.atm.dao.UserDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserDaoImpl userDao;

    public List<User> getAllUsers() {
        return userDao.getAll();
    }

    public User getUser(Integer id) {
        return userDao.getById(id);
    }

    public User addUser(User user) {
        return userDao.save(user);
    }
}
