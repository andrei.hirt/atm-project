package com.whitebox.atm.services;

import com.whitebox.atm.dao.BanknoteDaoImpl;
import com.whitebox.atm.models.Banknote;
import com.whitebox.atm.models.BanknoteType;
import com.whitebox.atm.models.FactoryBanknote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;

@Service
public class BanknoteService {
    @Autowired
    private BanknoteDaoImpl banknoteDao;

    public List<BanknoteType> getAtmCurrency() {
        return new ArrayList<>(EnumSet.allOf(BanknoteType.class));
    }

    public List<Banknote> getAtmFunds() {
        List<Banknote> banknotes = banknoteDao.getAll();
        Collections.sort(banknotes);
        return banknotes;
    }

    public Banknote getAtmBanknoteById(BanknoteType type) {
        return banknoteDao.getById(type);
    }

    public Banknote addBanknote(Banknote banknote) {
        return banknoteDao.save(banknote);
    }

    public List<Banknote> addListOfBanknotes(List<Banknote> banknoteList) {
        return banknoteDao.save(banknoteList);
    }

    public Banknote increaseBanknote(Banknote banknote) {
        return banknoteDao.increaseBanknote(banknote);
    }

    public void updateAtmFunds(List<Banknote> banknotesAtm, int[] countOfEachAtmBanknote) {
        banknoteDao.updateAtmFunds(banknotesAtm, countOfEachAtmBanknote);
    }

    public List<Banknote> getBanknotesWithdraw(int[] valueOfEachBanknote, int[] countOfEachBanknoteForAmount) {
        FactoryBanknote factoryBanknote = new FactoryBanknote();
        List<Banknote> banknotes = new ArrayList<>();
        Banknote resultBanknote;

        for (int noteIdx = 0; noteIdx < valueOfEachBanknote.length; noteIdx++) {
            if (countOfEachBanknoteForAmount[noteIdx] > 0) {
                resultBanknote = factoryBanknote.createBanknote(valueOfEachBanknote[noteIdx], countOfEachBanknoteForAmount[noteIdx]);
                banknotes.add(resultBanknote);
            }
        }
        return banknotes;
    }
}
