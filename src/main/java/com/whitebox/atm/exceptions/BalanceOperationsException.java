package com.whitebox.atm.exceptions;

public class BalanceOperationsException extends RuntimeException {
    public BalanceOperationsException(String message) {
        super(message);
    }
}
