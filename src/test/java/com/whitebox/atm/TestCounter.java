package com.whitebox.atm;

import com.whitebox.atm.models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCounter {
    Counter counter;
    BankAccount bankAccount;
    List<Banknote> banknotes;

    @BeforeEach
    void setup() {
        FactoryBanknote factoryBanknote = new FactoryBanknote();
        banknotes = new ArrayList<>();
        banknotes.add(factoryBanknote.createBanknote(1, 5));
        banknotes.add(factoryBanknote.createBanknote(5, 5));
        banknotes.add(factoryBanknote.createBanknote(10, 5));
        banknotes.add(factoryBanknote.createBanknote(20, 5));
        banknotes.add(factoryBanknote.createBanknote(50, 5));
        banknotes.add(factoryBanknote.createBanknote(100, 5));
        counter = new Counter();
        bankAccount = new BankAccount("5734BRD537", "Popescu Vasile", 1500);
    }

    @Test
    void should_calculate_correct_number_of_bank_notes_when_withdraw_amount_less_than_atm_funds() {
        counter.init(163, banknotes);
        int expectedNumberOfBanknotes = 6;

        counter.findMinimumNumberOfBanknotes();
        assertEquals(expectedNumberOfBanknotes, counter.getMinimumOfNotesForAmount(163));
    }

    @Test
    void should_calculate_correct_list_of_bank_notes_when_withdraw_amount_less_than_atm_funds() {
        counter.init(713, banknotes);
        int[] expectedCountOfEachBanknote = {3, 0, 1, 0, 4, 5};

        counter.findMinimumNumberOfBanknotes();
        counter.finishWithdrawal();
        assertArrayEquals(expectedCountOfEachBanknote, counter.getCountOfEachNoteForAmount());
    }

    @Test
    void should_calculate_zero_list_of_bank_notes_when_withdraw_amount_greater_than_atm_funds() {
        counter.init(931, banknotes);
        int[] expectedCountOfEachNote = {0, 0, 0, 0, 0, 0};

        counter.findMinimumNumberOfBanknotes();
        counter.finishWithdrawal();
        assertArrayEquals(expectedCountOfEachNote, counter.getCountOfEachNoteForAmount());
    }
}
