package com.whitebox.atm.controllers;

import com.whitebox.atm.models.User;
import static io.restassured.RestAssured.*;
import io.restassured.http.ContentType;
import static org.junit.jupiter.api.Assertions.*;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

public class TestUserController extends TestController{

    public void setupSaveOneUser() {
        User user = new User("Vasile", "Popescu");
        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/users")
                .then()
                .statusCode(201);
    }

    @Test
    public void should_return_success_when_get_all_users() {
        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .when()
                .get("/users")
                .then()
                .statusCode(200);
    }

    @Test
    public void should_return_correct_user_info_when_get_user_by_good_id() {
        setupSaveOneUser();

        Response response = given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .pathParam("id", 1)
                .when()
                .get("/users/{id}")
                .then()
                .statusCode(200)
                .extract()
                .response();

        assertEquals("1", response.jsonPath().getString("id"));
        assertEquals("Vasile", response.jsonPath().getString("firstName"));
        assertEquals("Popescu", response.jsonPath().getString("lastName"));
    }

    @Test
    public void should_return_bad_request_when_get_user_by_wrong_id() {
        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .pathParam("id", 100)
                .when()
                .get("/users/{id}")
                .then()
                .statusCode(404);
    }

    @Test
    public void should_return_bad_request_when_save_user_with_bad_fields() {
        User user = new User("Marian", "");
        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/users")
                .then()
                .statusCode(400);
    }
}
