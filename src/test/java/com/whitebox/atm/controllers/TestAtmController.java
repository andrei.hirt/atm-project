package com.whitebox.atm.controllers;

import com.whitebox.atm.models.*;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;

public class TestAtmController extends TestController {

    private Integer bankCardId;

    public void setup() {
        User user = new User("Vasile", "Popescu");
        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .post("/users")
                .then()
                .statusCode(201);

        BankAccount bankAccount = new BankAccount("6748BRD934", "Popescu Vasile", 1500);
        BankCard bankCard = new BankCard(new User("Vasile", "Popescu"), "1234 5678 8912 3456", "Popescu Vasile",
                "06/24","BRD", "567", "1234", bankAccount);
        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(bankCard)
                .when()
                .post("/users/1/bank-cards")
                .then()
                .statusCode(201);

        String banknoteList = "[\n" +
                "    {\n" +
                "        \"type\": \"1 RON\",\n" +
                "        \"count\": 5\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"5 RON\",\n" +
                "        \"count\": 0\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"10 RON\",\n" +
                "        \"count\": 0\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"20 RON\",\n" +
                "        \"count\": 5\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"50 RON\",\n" +
                "        \"count\": 3\n" +
                "    },\n" +
                "    {\n" +
                "        \"type\": \"100 RON\",\n" +
                "        \"count\": 5\n" +
                "    }\n" +
                "]";
        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .body(banknoteList)
                .when()
                .post("/atm/funds/list")
                .then()
                .statusCode(201);
    }

    public Integer getBankCardId() {
        Response response = given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .when()
                .get("/bank-cards")
                .then()
                .statusCode(200)
                .extract()
                .response();

        return Integer.parseInt(response.jsonPath().getString("[0].id"));
    }

    public boolean bankCardNotExists() {
        Response response = given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .when()
                .get("/bank-cards")
                .then()
                .statusCode(200)
                .extract()
                .response();

        JsonPath jsonPath = new JsonPath(response.asString());
        return jsonPath.get("size()").equals(0);
    }

    @Test
    public void should_return_correct_list_of_bank_notes_when_withdraw_amount() {
        int amountForWithdraw = 163;

        if (bankCardNotExists()) {
            setup();
        }

        bankCardId = getBankCardId();

        Response response = given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .queryParam("amount", amountForWithdraw)
                .pathParam("bankCardId", bankCardId)
                .when()
                .get("/atm/withdraw/{bankCardId}")
                .then()
                .statusCode(200)
                .extract()
                .response();

        String expectedListOfBanknoteString = "[{type={value=1, name=1 RON}, count=3, name=1 RON, value=1}, " +
                "{type={value=20, name=20 RON}, count=3, name=20 RON, value=20}, " +
                "{type={value=100, name=100 RON}, count=1, name=100 RON, value=100}]";

        Assertions.assertThat(response.jsonPath().getList("").toString()).isEqualTo(expectedListOfBanknoteString);
    }

    @Test
    public void should_return_success_when_deposit_amount() {
        int amountForDeposit = 163;

        if (bankCardNotExists()) {
            setup();
        }

        bankCardId = getBankCardId();

        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .queryParam("amount", amountForDeposit)
                .pathParam("bankCardId", bankCardId)
                .when()
                .post("/atm/deposit/{bankCardId}")
                .then()
                .statusCode(200);
    }

    @Test
    public void should_return_success_when_check_balance() {
        if (bankCardNotExists()) {
            setup();
        }

        bankCardId = getBankCardId();

        given()
                .port(super.port)
                .contentType(ContentType.JSON)
                .pathParam("bankCardId", bankCardId)
                .when()
                .get("/atm/check-balance/{bankCardId}")
                .then()
                .statusCode(200);
    }
}
