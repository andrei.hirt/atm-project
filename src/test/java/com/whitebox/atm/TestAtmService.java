package com.whitebox.atm;

import com.whitebox.atm.exceptions.BalanceOperationsException;
import com.whitebox.atm.models.*;
import com.whitebox.atm.services.AtmService;
import com.whitebox.atm.services.BankAccountService;
import com.whitebox.atm.services.BankCardService;
import com.whitebox.atm.services.BanknoteService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

public class TestAtmService {
    private List<Banknote> banknotes;

    private BankCard bankCard;

    private BankAccount bankAccount;

    @InjectMocks
    private AtmService atmService;

    @Mock
    private BanknoteService banknoteService;

    @Mock
    private BankAccountService bankAccountService;

    @Mock
    private BankCardService bankCardService;

    @BeforeEach
    void setup() {
        MockitoAnnotations.openMocks(this);

        FactoryBanknote factoryBanknote = new FactoryBanknote();
        banknotes = new ArrayList<>();
        banknotes.add(factoryBanknote.createBanknote(1, 5));
        banknotes.add(factoryBanknote.createBanknote(5, 0));
        banknotes.add(factoryBanknote.createBanknote(10, 0));
        banknotes.add(factoryBanknote.createBanknote(20, 5));
        banknotes.add(factoryBanknote.createBanknote(50, 3));
        banknotes.add(factoryBanknote.createBanknote(100, 5));

        bankAccount = new BankAccount("6748BRD934", "Popescu Vasile", 1500);
        bankAccount.setId(1);
        bankCard = new BankCard(new User("Vasile", "Popescu"), "1234 5678 8912 3456", "Popescu Vasile",
                "06/24","BRD", "567", "1234", bankAccount);
        bankCard.setId(1);

        when(bankCardService.getBankCard(bankCard.getId())).thenReturn(bankCard);
        when(banknoteService.getAtmFunds()).thenReturn(banknotes);
    }

    @Test
    public void should_return_correct_list_of_bank_notes_when_withdraw_amount() {
        int amountForWithdraw = 713;
        FactoryBanknote factoryBanknote = new FactoryBanknote();
        List<Banknote> expectedListOfBanknote = Arrays.asList(
                factoryBanknote.createBanknote(1,3),
                factoryBanknote.createBanknote(20, 3),
                factoryBanknote.createBanknote(50, 3),
                factoryBanknote.createBanknote(100, 5));

        doAnswer(invocation -> {
                    Integer id = invocation.getArgument(0);
                    int newBalance = invocation.getArgument(1);

                    assertEquals(bankAccount.getId(), id);
                    assertEquals(bankAccount.getBalance() - amountForWithdraw, newBalance);

                    return null;})
                .when(bankAccountService).updateBankAccountBalance(anyInt(), anyInt());

        when(banknoteService.getBanknotesWithdraw(any(int[].class), any(int[].class))).thenReturn(expectedListOfBanknote);

        Assertions.assertThat(atmService.withdraw(bankCard.getId(), amountForWithdraw))
                .isEqualTo(expectedListOfBanknote);
    }

    @Test
    public void should_return_exception_when_withdraw_amount_greater_than_atm_funds() {
        int amountForWithdraw = 937;

        assertThrows(BalanceOperationsException.class, () -> { atmService.withdraw(bankCard.getId(), amountForWithdraw); });
    }

    @Test
    public void should_return_correct_list_of_bank_notes_when_withdraw_amount_is_bad_for_greedy() {
        int amountForWithdraw = 163;

        FactoryBanknote factoryBanknote = new FactoryBanknote();
        List<Banknote> expectedListOfBanknote = Arrays.asList(
                factoryBanknote.createBanknote(1, 3),
                factoryBanknote.createBanknote(20, 3),
                factoryBanknote.createBanknote(100, 1));

        doAnswer(invocation -> {
            Integer id = invocation.getArgument(0);
            int newBalance = invocation.getArgument(1);

            assertEquals(bankAccount.getId(), id);
            assertEquals(bankAccount.getBalance() - amountForWithdraw, newBalance);

            return null;})
                .when(bankAccountService).updateBankAccountBalance(anyInt(), anyInt());

        when(banknoteService.getBanknotesWithdraw(any(int[].class), any(int[].class))).thenReturn(expectedListOfBanknote);

        Assertions.assertThat(atmService.withdraw(bankCard.getId(), amountForWithdraw))
                .isEqualTo(expectedListOfBanknote);
    }

    @Test
    public void should_throw_exception_when_withdraw_negative_amount() {
        int amountForWithdraw = -250;

        assertThrows(BalanceOperationsException.class, () -> { atmService.withdraw(bankCard.getId(), amountForWithdraw); });
    }

    @Test
    public void should_throw_exception_when_withdraw_zero_amount() {
        int amountForWithdraw = 0;

        assertThrows(BalanceOperationsException.class, () -> { atmService.withdraw(bankCard.getId(), amountForWithdraw); });
    }

    @Test
    public void should_throw_exception_when_withdraw_amount_greater_than_bank_account_balance() {
        int amountForWithdraw = 1650;

        assertThrows(BalanceOperationsException.class, () -> { atmService.withdraw(bankCard.getId(), amountForWithdraw); });
    }

    @Test
    public void should_return_true_when_deposit_amount() {
        int amountForDeposit = 500;

        doAnswer(invocation -> {
            Integer id = invocation.getArgument(0);
            int newBalance = invocation.getArgument(1);

            assertEquals(bankAccount.getId(), id);
            assertEquals(bankAccount.getBalance() + amountForDeposit, newBalance);

            return null;})
                .when(bankAccountService).updateBankAccountBalance(anyInt(), anyInt());

        assertEquals(amountForDeposit, atmService.deposit(bankCard.getId(), amountForDeposit));
    }

    @Test
    public void should_throw_exception_when_deposit_negative_amount() {
        int amountForDeposit = -500;

        assertThrows(BalanceOperationsException.class, () -> { atmService.deposit(bankCard.getId(), amountForDeposit); });
    }

    @Test
    public void should_throw_exception_when_deposit_zero_amount() {
        int amountForDeposit = 0;

        assertThrows(BalanceOperationsException.class, () -> { atmService.deposit(bankCard.getId(), amountForDeposit); });
    }

    @Test
    public void should_return_balance_when_check_balance() {
        assertEquals(bankAccount.getBalance(), atmService.checkBalance(bankCard.getId()));
    }
}
